#include "paintbuckethandler.h"
#include <QImage>

#include<QDebug>
#include<QPair>
#include<QList>
#include<QFile>
PaintBucketHandler::PaintBucketHandler(QObject *parent) : QObject(parent)
{

}

void PaintBucketHandler::UsePaintBucket(int XPos, int YPos, QString ImageFile, int NewR, int NewG, int NewB )
{
    QImage imageFile;

    if ( !imageFile.load( ImageFile ) )
    {
        return;
    }

    imageFile = imageFile.convertToFormat(QImage::Format_RGBA8888);

    uchar *bits = imageFile.bits();

    int origR, origG, origB;
    int begPixel = XPos * 4 + YPos * 4 * imageFile.width();

    origR = bits[ begPixel ];
    origG = bits[ begPixel + 1];
    origB = bits[ begPixel + 2 ];



    bool reachLeft( false );
    bool reachRight( false );

    QList< QPair<int, int> > pixelStack;
    pixelStack.push_back( QPair< int, int>( XPos, YPos ) );
    while( pixelStack.length() > 0 )
    {
        reachLeft = false;
        reachRight = false;

        QPair<int, int> currentPixel = pixelStack[0];
        pixelStack.pop_front();
        begPixel = currentPixel.first * 4 + currentPixel.second * 4 * imageFile.width();

        while( currentPixel.second - 1 >= 0 )
        {
            begPixel = currentPixel.first * 4 + currentPixel.second * 4 * imageFile.width();
            if( !( ( bits[ begPixel ] == origR ) && ( bits[ begPixel + 1 ] == origG ) && ( bits[ begPixel + 2] == origB) ) )
            {
                break;
            }
            currentPixel.second--;
        }

        currentPixel.second++;
        begPixel = currentPixel.first * 4 + currentPixel.second * 4 * imageFile.width();
        bits[ begPixel ] = NewR;
        bits[ begPixel + 1 ] = NewG;
        bits[ begPixel + 2 ] = NewB;
        bits[ begPixel + 3 ] = 255;

        while( currentPixel.second + 1 < imageFile.height() )
        {
            currentPixel.second++;
            begPixel = currentPixel.first * 4 + currentPixel.second * 4 * imageFile.width();
            if( ( ( bits[ begPixel ] == origR ) && ( bits[ begPixel + 1 ] == origG ) && ( bits[ begPixel + 2] == origB) ) )
            {
                bits[ begPixel ] = NewR;
                bits[ begPixel + 1 ] = NewG;
                bits[ begPixel + 2 ] = NewB;
                bits[ begPixel + 3 ] = 255;

                if( currentPixel.first > 1 )
                {
                    int leftPixel = ( currentPixel.first - 1 ) * 4 + currentPixel.second * 4 * imageFile.width();
                    if( ( ( bits[ leftPixel ] == origR ) && ( bits[ leftPixel + 1 ] == origG ) && ( bits[ leftPixel + 2] == origB) ) )
                    {
                        if( !reachLeft )
                        {
                            pixelStack.push_back(QPair< int, int> ( currentPixel.first - 1, currentPixel.second ) );
                            reachLeft = true;
                        }
                    }
                    else
                    {
                        reachLeft = false;
                    }
                }

                if ( currentPixel.first < imageFile.width() )
                {
                    int rightPixel = ( currentPixel.first + 1 ) * 4 + currentPixel.second * 4 * imageFile.width();
                    if( ( ( bits[ rightPixel ] == origR ) && ( bits[ rightPixel + 1 ] == origG ) && ( bits[ rightPixel + 2] == origB) ) )
                    {
                        if( !reachRight )
                        {
                            pixelStack.push_back(QPair< int, int> ( currentPixel.first + 1, currentPixel.second ) );
                            reachRight = true;
                        }
                    }
                    else
                    {
                        reachRight = false;
                    }
                }
            }
            else
            {
                break;
            }
        }
    }
    if( QFile( "/Users/hpham/Desktop/save2.png" ).exists())
    {
        QFile("/Users/hpham/Desktop/save2.png").remove();
    }

    QImage newImage( bits, imageFile.width(), imageFile.height(), QImage::Format_RGBA8888 );
    newImage.save("/Users/hpham/Desktop/save2.png", "PNG");
    emit finishedBucket( "file:///Users/hpham/Desktop/save2.png");
}
