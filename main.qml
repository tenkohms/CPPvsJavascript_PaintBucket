import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2

import PaintBuckerHandler 1.0

Window {
    visible: true
    width: Screen.width
    height: Screen.height

    property int startX
    property int startY
    property int finishX
    property int finishY
    property string brushColor: "black"
    property bool filled: false
    property bool brushTool: true
    property bool fillTool: false
    property bool isJScript: false
    property bool isBucketDone: true
    property string pbImage

    PaintBucket {
        id: cppPaintBucket
        onFinishedBucket: {
            console.log( new Date().toLocaleTimeString() )
            pbImage = imageFile.toString()
            myCanvas.loadImage( imageFile )
        }
    }

    Text{
        id: colorHolder
        visible: false
        color: "green"
    }

    Rectangle {
        anchors.fill: myCanvas
    }

    Canvas {
        id: myCanvas
        anchors.fill: parent

        property variant holder: []
        property var undoImage;
        property var redoImage;
        property int holderIndex: -1;

        function paintBucket()
        {
            console.log( new Date().toLocaleTimeString() )
            isBucketDone = false;
            var reachLeft = false;
            var reachRight = false;
            var sX = parseInt( startX, 10 )
            var sY = parseInt( startY, 10 )
            var pixelStack = [[startX, startY]]
            var ctx = myCanvas.getContext('2d')

            var colorLayer = ctx.getImageData(0, 0,myCanvas.width, myCanvas.height )
            var begPixel = sX * 4 + sY * 4 * colorLayer.width
            var r1 = colorLayer.data[begPixel ]
            var g1 = colorLayer.data[begPixel + 1]
            var b1 = colorLayer.data[ begPixel + 2 ]



            if ( r1 === ( colorHolder.color.r * 255 ) && g1 === ( colorHolder.color.g * 255 ) && b1 === ( colorHolder.color.b * 255 ) )
            {
                isBucketDone = true;
                return;

            }

            var  r2, b2, g2, newIndex, oPixel

            while( pixelStack.length )
            {
                var pixelToCheck = pixelStack.pop()
                sY = pixelToCheck[1]
                sX = pixelToCheck[0]
                begPixel = sX * 4 + sY * 4 * colorLayer.width

                reachLeft = false;
                reachRight = false;

                while ( sY - 1 >= 0 )
                {
                    begPixel = sX * 4 + ( sY  ) * 4 * colorLayer.width

                    if ( !( ( colorLayer.data[begPixel ] === r1 ) && ( colorLayer.data[begPixel + 1] === g1) && ( colorLayer.data[ begPixel + 2 ] === b1 ) ) )
                    {
                        break;
                    }
                    sY = sY - 1

                }

                sY = sY + 1;
                ctx.fillRect( sX, sY, 2, 2)
                begPixel = sX * 4 + sY * 4 * colorLayer.width
                colorLayer.data[ begPixel ] = colorHolder.color.r * 255
                colorLayer.data[ begPixel + 1 ] = colorHolder.color.g * 255
                colorLayer.data[ begPixel + 2 ] = colorHolder.color.b * 255
                colorLayer.data[ begPixel + 3 ] = 255

                while ( sY + 1 < colorLayer.height )
                {
                    sY = sY + 1
                    begPixel = sX * 4 + sY * 4 * colorLayer.width
                    if ( ( ( colorLayer.data[begPixel ] === r1 ) && ( colorLayer.data[begPixel + 1] === g1) && ( colorLayer.data[ begPixel + 2 ] === b1 ) ) )
                    {
                        ctx.fillRect( sX, sY, 2, 2)
                        colorLayer.data[ begPixel ] = colorHolder.color.r * 255
                        colorLayer.data[ begPixel + 1 ] = colorHolder.color.g * 255
                        colorLayer.data[ begPixel + 2 ] = colorHolder.color.b * 255
                        colorLayer.data[ begPixel + 3 ] = 255

                        if ( sX > 1 )
                        {
                            oPixel = ( sX - 1 ) * 4 + sY * 4 * colorLayer.width
                            if ( ( colorLayer.data[oPixel ] === r1 ) && ( colorLayer.data[oPixel + 1] === g1) && ( colorLayer.data[ oPixel + 2 ] === b1 ) )
                            {
                                if ( !reachLeft )
                                {
                                    pixelStack.push( [ sX -1, sY] )
                                    reachLeft = true;
                                }
                            }
                            else
                            {
                                reachLeft = false;
                            }
                        }

                        if ( sX < myCanvas.width )
                        {
                            oPixel = ( sX + 1) * 4 + sY * 4 * colorLayer.width
                            if ( ( colorLayer.data[oPixel ] === r1 ) && ( colorLayer.data[oPixel + 1] === g1) && ( colorLayer.data[ oPixel + 2 ] === b1 ) )
                            {
                                if ( !reachRight )
                                {
                                    pixelStack.push( [ sX + 1, sY] )
                                    reachRight = true;
                                }
                            }
                            else
                            {
                                reachRight = false;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }

            ctx.drawImage( colorLayer, 0, 0)
            myCanvas.requestPaint()
            startX = -1
            startY = -1
            finishX = -1
            finishY = -1

            console.log( new Date().toLocaleTimeString() )

        }

        onImageLoaded: {
            var ctx = getContext("2d")
            ctx.globalAlpha = myCanvas.alpha;
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, myCanvas.width, myCanvas.height);
            ctx.drawImage( pbImage, 0, 0, myCanvas.width, myCanvas.height)

            startX = -1;
            startY = -1;
            finishX = -1;
            finishY = -1;
            myCanvas.requestPaint()
        }

        onPaint: {
            if ( !filled )
            {
                var ctx = getContext("2d");
                ctx.globalAlpha = myCanvas.alpha;
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, myCanvas.width, myCanvas.height);
                filled = true;
            }
            if( brushTool)
            {
                var ctx = getContext('2d')
                ctx.beginPath();
                ctx.strokeStyle = brushColor;
                ctx.lineWidth = 12;
                ctx.lineJoin = "round";
                ctx.moveTo(startX, startY);
                ctx.lineTo(finishX, finishY);
                ctx.closePath();
                ctx.stroke();
                startX = finishX;
                startY = finishY;
            }

            if ( fillTool )
            {
                isBucketDone = true
            }
        }
    }

    MouseArea {
        anchors.fill: myCanvas
        onPressed: {
            startX = mouseX
            startY = mouseY
        }
        onPositionChanged: {
            finishX = mouseX
            finishY = mouseY
            myCanvas.requestPaint()
        }

        onClicked: {
            if ( fillTool && isBucketDone )
            {
                startX = mouseX
                startY = mouseY
                if ( isJScript )
                {
                    myCanvas.paintBucket()
                }
                else
                {
                    myCanvas.save( "/Users/hpham/Desktop/save.bmp")
                    myCanvas.unloadImage(pbImage)
                    console.log( new Date().toLocaleTimeString() )
                    cppPaintBucket.UsePaintBucket( startX, startY, "/Users/hpham/Desktop/save.bmp", colorHolder.color.r * 255, colorHolder.color.g * 255, colorHolder.color.b * 255)
                }
            }
        }
    }

    Button {
        id: brushToolButton
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        text: "Brush"
        onClicked: {
            brushTool = true
            fillTool = false
        }
    }

    Button {
        id: jScriptButton
        anchors.top: brushToolButton.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        text: "jScriptFillTool"
        onClicked: {
            brushTool = false
            fillTool = true
            isJScript = true
        }
    }

    Button {
        id: cppButton
        anchors.top: jScriptButton.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        text: "cppFillTool"
        onClicked: {
            brushTool = false
            fillTool = true
            isJScript = false
        }
    }

}
