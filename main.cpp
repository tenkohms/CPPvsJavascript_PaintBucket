#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "paintbuckethandler.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType< PaintBucketHandler > ( "PaintBuckerHandler", 1, 0, "PaintBucket");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
